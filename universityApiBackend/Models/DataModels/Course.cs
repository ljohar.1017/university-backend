﻿using System.ComponentModel.DataAnnotations;

namespace universityApiBackend.Models.DataModels
{
    public class Course : BaseEntity
    {
        [Required, StringLength(50)]
        public string Name { get; set; } = string.Empty;

        [Required, StringLength(280)]
        public string Synopsis { get; set; } = string.Empty;

        public string? Description { get; set; }

        [Required]
        public string TargetAudiences { get; set; } = string.Empty;

        public string? Objectives { get; set; }

        [Required]
        public string Requisites { get; set; } = string.Empty;

        [Required]
        public Level Level { get; set; } = Level.Basic;
        
    }

    public enum Level{
        Basic,
        Intermediate,
        Advanced
    }
}
